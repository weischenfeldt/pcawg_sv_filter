#!/usr/bin/env cwl-runner

class: CommandLineTool
id: pcawg_sv_filter
label: pcawg_sv_filter_workflow
cwlVersion: v1.0

doc: |
    ![build_status](https://....)
    A Docker container for the pcawg_sv_filter_workflow.

dct:creator:
  '@id': http://orcid.org/0000-0003-3684-2659
  foaf:name: Francesco Favero
  foaf:mbox: francesco.favero@bric.ku.dk

dct:contributor:
  foaf:name: Etsehiwot Girum Girma
  foaf:mbox: Etsehiwot@finsenlab.dk

requirements:
  - class: DockerRequirement
    dockerPull: essi/pcawg_sv_filter:testing

hints:
  - class: ResourceRequirement
    coresMin: 2
    ramMin: 4092
    outdirMin: 512000

inputs:
  run_id:
    type: string
    inputBinding:
      position: 1
      prefix: --run-id
  somatic:
    type: File
    inputBinding:
      position: 2
      prefix: --somatic
  germline:
    type: File
    inputBinding:
      position: 3
      prefix: --germline
  readname:
    type: File
    inputBinding:
      position: 5
      prefix: --readname
outputs:
  readname_txt:
    type: File
    outputBinding:
      glob: 'filter_calls/*.readname.txt'
  svFreq_bedpe_txt:
    type: File
    outputBinding:
      glob: 'filter_calls/*.svFreq.bedpe.txt'
  svFreq_germline_bedpe_txt:
    type: File
    outputBinding:
      glob: 'filter_calls/*.svFreq.germline.bedpe.txt'
  svFreq_germline_highConf_bedpe_txt:
    type: File
    outputBinding:
      glob: 'filter_calls/*.svFreq.germline.highConf.bedpe.txt'
  svFreq_germline_highConf_vcf_gz:
    type: File
    outputBinding:
      glob: 'filter_calls/*.svFreq.germline.highConf.vcf.gz'
  svFreq_germline_highConf_vcf_gz_tbi:
    type: File
    outputBinding:
      glob: 'filter_calls/*.svFreq.germline.highConf.vcf.gz.tbi'
  svFreq_germline_vcf_gz:
    type: File
    outputBinding:
      glob: 'filter_calls/*.svFreq.germline.vcf.gz'
  svFreq_germline_vcf_gz_tbi:
    type: File
    outputBinding:
      glob: 'filter_calls/*.svFreq.germline.vcf.gz.tbi'
  svFreq_somatic_bedpe_txt:
    type: File
    outputBinding:
      glob: 'filter_calls/*.svFreq.somatic.bedpe.txt'
  svFreq_somatic_highConf_bedpe_txt:
    type: File
    outputBinding:
      glob: 'filter_calls/*.svFreq.somatic.highConf.bedpe.txt'
  svFreq_somatic_highConf_vcf_gz:
    type: File
    outputBinding:
      glob: 'filter_calls/*.svFreq.somatic.highConf.vcf.gz'
  somatic_sv_vcf_gz:
    type: File
    outputBinding:
      glob: 'filter_calls/*.somatic.sv.vcf.gz'
  somatic_sv_vcf_gz_md5:
    type: File
    outputBinding:
      glob: 'filter_calls/*.somatic.sv.vcf.gz.md5'
  somatic_sv_vcf_gz_tbi:
    type: File
    outputBinding:
      glob: 'filter_calls/*.somatic.sv.vcf.gz.tbi'
  somatic_sv_vcf_gz_tbi_md5:
    type: File
    outputBinding:
      glob: 'filter_calls/*.somatic.sv.vcf.gz.tbi.md5'
  svFreq_somatic_vcf_gz:
    type: File
    outputBinding:
      glob: 'filter_calls/*.svFreq.somatic.vcf.gz'
  svFreq_somatic_vcf_gz_tbi:
    type: File
    outputBinding:
      glob: 'filter_calls/*.svFreq.somatic.vcf.gz.tbi'
  svFreq_vcf_gz:
    type: File
    outputBinding:
      glob: 'filter_calls/*.svFreq.vcf.gz'
  svFreq_vcf_gz_tbi:
    type: File
    outputBinding:
      glob: 'filter_calls/*.svFreq.vcf.gz.tbi'
  vcf_gz:
    type: File
    outputBinding:
      glob: 'filter_calls/*.raw.vcf.gz'
  vcf_gz_tbi:
    type: File
    outputBinding:
      glob: 'filter_calls/*.raw.vcf.gz.tbi'

baseCommand: [/usr/bin/run_sv_filter.py]
