# -*- coding: utf-8 -*-
"""
Created on Tue Jan 19 2016

@author: joachim.weischenfeldt 
"""

import csv, os, sys, gzip, subprocess, time

vcfFile = sys.argv[1]
if not vcfFile.endswith('.vcf.gz'):
    print "file must end with .vcf.gz. Exit"
    sys.exit(-1)
vcfFileBak = vcfFile.replace(".vcf.gz", "_original.vcf.bak")
vcfFileOut = vcfFile.replace(".vcf.gz", ".tmp.vcf")
with gzip.open(vcfFile) as vcfIn, open(vcfFileOut, 'w') as vcfOut:
    for f in vcfIn:
        row = f.rsplit()
        if row[0].startswith('##'):
            vcfOut.writelines(' '.join(map(str, row)) + "\n")
        else:
            newrow = row[:9] + row[10:] + [row[9]]
            vcfOut.writelines('\t'.join(map(str, newrow)) + "\n")

cmdZip = "bgzip -f " + vcfFileOut
subprocess.Popen(cmdZip.split())
vcfFileOut = vcfFileOut + ".gz"
time.sleep(3)
cmdTab = "tabix -f -p vcf " + vcfFileOut
subprocess.Popen(cmdTab .split())
time.sleep(3)
os.rename(vcfFile, vcfFileBak)
os.rename(vcfFileOut, vcfFile)
print ("new vcf file: {0}\n\nOld vcf file renamed: {1}\n".format(vcfFile, vcfFileBak))

