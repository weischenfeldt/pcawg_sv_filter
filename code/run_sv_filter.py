#!/usr/bin/env python2.7

import argparse
import os
import shlex
import subprocess
import logging


class ExtLog:

    def __init__(self, name, path, level):
        self.__name__ = name
        self.__path__ = path
        if not os.path.exists(self.__path__):
            os.makedirs(self.__path__)
        self.log = logging.getLogger(name)
        self.log_file = os.path.join(self.__path__, '%s.log' % self.__name__)
        formatter = logging.Formatter(
            '%(levelname)s : %(asctime)s : %(message)s')
        fileHandler = logging.FileHandler(self.log_file, mode='w')
        fileHandler.setFormatter(formatter)
        streamHandler = logging.StreamHandler()
        streamHandler.setFormatter(formatter)
        self.log.setLevel(level)
        self.log.addHandler(fileHandler)
        self.log.addHandler(streamHandler)


def main():
    parser = argparse.ArgumentParser(description='Filter and Merge sv calls')
    parser.add_argument('--run-id',  dest='run_id',
                        help='Sample id, identifier of the run', required=True)
    parser.add_argument('--somatic',  dest='delly_soma',
                        help='DELLY SOMATIC VCF',  required=True)
    parser.add_argument('--germline',  dest='delly_germ',
                        help='DELLY GERMLINE VCF', required=True)
    parser.add_argument('--email',  dest='email',
                        help='EMAIL',  required=False)
    parser.add_argument('--readname',  dest='delly_readname',
                        help='DELLY READNAME FILE',  required=True)

    args = parser.parse_args()
    current_dir = os.getcwd()
    log_dir = os.path.join(current_dir, 'logs')
    log = ExtLog('run_dockstore', log_dir, level=logging.INFO)
    log.log.info('Output results in folder %s' % current_dir)
    input_path = os.path.join(current_dir, 'data')
    log.log.info('Create input data folder: %s' % input_path)
    if not os.path.exists(input_path):
        os.makedirs(input_path)
    else:
        log.log.warning('Data folder (%s) already exists' % input_path)


    filter_cmd = ['annotateMergedSvCalls_delly_pcawg_crome.sh', args.run_id, args.delly_soma, args.delly_germ, args.email, args.delly_readname]
    filter_cmd = shlex.split(' '.join(map(str, filter_cmd)))
    log.log.info('Prepare the pcawg filter sv command line:')
    log.log.info(' '.join(map(str, filter_cmd)))
    filter_proc = subprocess.Popen(filter_cmd)
    out = filter_proc.communicate()[0]
    code = filter_proc.returncode
    info = 'annotateMergedSvCalls_delly_pcawg_crome.sh, exit code: %s' % code
    if code == 0:
        log.log.info(info)
    else:
        log.log.error(info)


if __name__ == '__main__':
    main()
