#!/bin/bash
aliquot_id=$1
SAMPLE_SOMA=$2
SAMPLE_GERM=$3
#EMAIL=$4
SAMPLE_READNAME=$5

DIR=${HOME}
DATA_DIR=${DIR}/data
SAMPLE_NAME=${aliquot_id}
echo $SAMPLE_NAME
DIR_PROJECT=${DIR}/filter_calls
mkdir -p $DIR_PROJECT
echo ${DIR_PROJECT}
ETC_PCAWG_DIR=/etc/pcawg_filter_sv
PCAWG_SV_CALLCOLLECTION_DIR=/usr/svCallCollection
DIR_SV_COLLECTION="${PCAWG_SV_CALLCOLLECTION_DIR}/svCallCollectionsPCAWG.tar.gz"
GENCODE="${ETC_PCAWG_DIR}/files/gencode.v19.protein_coding_gene.minimal.bed.gz"
echo $SAMPLE_SOMA $SAMPLE_GERM

CUTOFF_SV_OVERHANG=1000
CODE_DIR=/opt/code
BIN_DIR=/opt/bin

BEDTOOLS=${BIN_DIR}/bedtools
VCF2TSV=${BIN_DIR}/vcf2tsv
VCFADDINFO=${BIN_DIR}/vcfaddinfo
DELLY2BED="${CODE_DIR}/dellyVcf2Tsv.py"
SOMATICFILTER="${CODE_DIR}/DellySomaticFreqFilter.py"
VCF2BND="${CODE_DIR}/dellyVcf2BndVcf.sh"
VCFSAMPLENAMES=${BIN_DIR}/vcfsamplenames
svSuffix=sv.vcf.gz

SV_FILE_BASE=$DIR_PROJECT/$SAMPLE_NAME

SV_FILE=${SV_FILE_BASE}.raw.vcf
ls $SAMPLE_SOMA $SAMPLE_GERM | xargs vcf-concat | vcf-sort   > ${SV_FILE}
bgzip -f ${SV_FILE}
SV_FILE=${SV_FILE}.gz
tabix -f -p vcf ${SV_FILE}
echo ${SV_FILE}

echo -e "merged vcf\n" `ls -lh ${SV_FILE}`
####################################################################################################
## bugfix. Some of the  DOCKER runs produced vcf files with normal as the first genotype. Change order
## to put tumor first, then normal - needed for somatic filtering scripts. Will add .bak to old vcf file

normalBamNames="${ETC_PCAWG_DIR}/files/PCAWG_March_2016_Dataset_v1_normal_bam.txt"

if (  grep $($VCFSAMPLENAMES ${SV_FILE} | head -1).bam $normalBamNames > /dev/null ||  $VCFSAMPLENAMES ${SV_FILE} | head -1 | grep -P "^control"  > /dev/null);
then
if [[  $($VCFSAMPLENAMES ${SV_FILE} | head -1 | wc -c)  -gt 1  ]];
then
echo "normal is first. Need to reverse"
$VCFSAMPLENAMES ${SV_FILE}
python ${CODE_DIR}/reorderVcfSample.py ${SV_FILE}
fi
fi

####################################################################################################

cp ${SV_FILE}  ${SV_FILE_BASE}.tmp.vcf.gz
tabix -f -p vcf ${SV_FILE_BASE}.tmp.vcf.gz
tabix -f -p vcf ${SV_FILE}
$VCF2TSV -n "." ${SV_FILE} | awk 'BEGIN{ OFS="\t" }{ if (NR==1) { for (i=1; i<=NF; i++) { c[$i]=i };} print $c["CHROM"],$c["POS"]-'$CUTOFF_SV_OVERHANG',$c["POS"]+'$CUTOFF_SV_OVERHANG',$c["CHR2"],$c["END"]-'$CUTOFF_SV_OVERHANG',$c["END"]+'$CUTOFF_SV_OVERHANG',$c["ID"],
$c["MAPQ"], $c["CT"],$c["ID"],NR,".",".",".",$c["ID"],$c["CHROM"]"%",$c["POS"] }' | sed 's/\.fa\t/\t/g;s/chr//;s/chr//;s/3to5/+\t-/;s/5to3/-\t+/;s/3to3/+\t+/;s/5to5/-\t-/' | sed '1d'  > ${SV_FILE_BASE}.tmp.bedpe
tar -zxvf $DIR_SV_COLLECTION

for j in $HOME/svCallCollectionsPCAWG/1KGP/ $HOME/svCallCollectionsPCAWG/*/{tumor,control}
do
if [[ $(basename $j) == "1KGP" ]]
then
COMPARISON_SAMPLE_SET="1KGP"
count=1
for i in ${j}/combinedSVs.bedpe.part*
do
echo $i
$BEDTOOLS pairtopair  -is -a ${SV_FILE_BASE}.tmp.bedpe -b $i > ${SV_FILE_BASE}.tmp.bedpe.overlap.${COMPARISON_SAMPLE_SET}.p${count}
count=$(( $count + 1 ))
done
else

COMPARISON_SAMPLE_SET="$(basename $(dirname $j))_$(basename $j)"
echo $j
count=1
for i in ${j}/combinedSVs.bedpe.part*
do
echo $i
$BEDTOOLS pairtopair  -is -a ${SV_FILE_BASE}.tmp.bedpe -b $i > ${SV_FILE_BASE}.tmp.bedpe.overlap.${COMPARISON_SAMPLE_SET}.p${count}
count=$(( $count + 1 ))
done
fi

if [[ -f ${SV_FILE_BASE}.tmp.bedpe.overlap.${COMPARISON_SAMPLE_SET}.p1 ]]; then
cat ${SV_FILE_BASE}.tmp.bedpe.overlap.${COMPARISON_SAMPLE_SET}.p* | cut -f1-18,25 | sort | uniq | cut -f1-18 | cat -  ${SV_FILE_BASE}.tmp.bedpe | sort -k7n | uniq -c | awk 'BEGIN{ OFS="\t" }{ $1=$1-1; print }' > ${SV_FILE_BASE}.tmp.bedpe.overlap.${COMPARISON_SAMPLE_SET}.sampleCount

fi

cat $j/numberOfSamples.txt > ${SV_FILE_BASE}.tmp.bedpe.overlap.${COMPARISON_SAMPLE_SET}.totalNumSamples

cat  ${SV_FILE_BASE}.tmp.bedpe.overlap.${COMPARISON_SAMPLE_SET}.totalNumSamples

zgrep "^#" ${SV_FILE} | sed 's/FILTER\tINFO\tFORMAT.*/FILTER\tINFO/' | sed 's/\(##INFO=<ID=CIEND.*\)/##INFO=<ID=SC_'$COMPARISON_SAMPLE_SET'_C,Number=1,Type=Float,Description="Count of variant in '$COMPARISON_SAMPLE_SET'">\n##INFO=<ID=SC_'$COMPARISON_SAMPLE_SET'_F,Number=1,Type=Float,Description="Frequency of variant in '$COMPARISON_SAMPLE_SET'">\n\1/' > ${SV_FILE_BASE}.tmp.bedpe.overlap.${COMPARISON_SAMPLE_SET}.sampleCount.vcf
cat ${SV_FILE_BASE}.tmp.bedpe.overlap.${COMPARISON_SAMPLE_SET}.sampleCount | awk -v sampleCount=`cat ${SV_FILE_BASE}.tmp.bedpe.overlap.${COMPARISON_SAMPLE_SET}.totalNumSamples` 'BEGIN{ OFS="\t" }{ print $18,$19,$17,".",".",".",".","SC_'$COMPARISON_SAMPLE_SET'_C="$1";SC_'$COMPARISON_SAMPLE_SET'_F="int(($1/sampleCount)*10000)/10000 }' | sed 's/%//' >> ${SV_FILE_BASE}.tmp.bedpe.overlap.${COMPARISON_SAMPLE_SET}.sampleCount.vcf

$VCFADDINFO ${SV_FILE_BASE}.tmp.vcf.gz <(vcf-sort ${SV_FILE_BASE}.tmp.bedpe.overlap.${COMPARISON_SAMPLE_SET}.sampleCount.vcf)  > ${SV_FILE_BASE}.svFreq.vcf

cp ${SV_FILE_BASE}.svFreq.vcf ${SV_FILE_BASE}.tmp.vcf
bgzip -f ${SV_FILE_BASE}.tmp.vcf
tabix -f -p vcf ${SV_FILE_BASE}.tmp.vcf.gz
rm ${SV_FILE_BASE}.tmp.bedpe.overlap.${COMPARISON_SAMPLE_SET}*
done

rm ${SV_FILE_BASE}.tmp.*
date

if [[ -f ${SV_FILE_BASE}.svFreq.vcf ]];then
python ${SOMATICFILTER} -v ${SV_FILE_BASE}.svFreq.vcf
for vcf in ${SV_FILE_BASE}.svFreq.*vcf; do
echo $vcf
$DELLY2BED -v ${vcf} -o ${vcf/.vcf*/.bedpe.txt}
bgzip -f $vcf
tabix -f -p vcf ${vcf}.gz
done
fi
## collect readname files
READNAME=${SV_FILE_BASE}.readname.txt
mkdir rn_tmp
#rm rn_tmp/*
find $SAMPLE_READNAME -maxdepth 2 -iname  "${SAMPLE_NAME}*preFilter*somatic.sv.readname.txt.tar.gz" -exec cp {} ./rn_tmp \;
if echo ./rn_tmp/*tar.gz > /dev/null;
then
tar -xvzf ./rn_tmp/*tar.gz -C ./rn_tmp
find ./rn_tmp/ -iname  "*.pe.dump.txt" | xargs cat {} > ${READNAME}
#rm -r rn_tmp/
fi
echo "filter and reformat to BND format"
bash ${VCF2BND} ${SV_FILE_BASE}.svFreq.somatic.highConf.vcf.gz ${EMAIL} ${READNAME}
