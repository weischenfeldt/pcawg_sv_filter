#!/usr/bin/env python
## created: 141119
## by: Joachim Weischenfeldt

# /home/weischej/job_scripts/pawg_scripts/PAWG/delly_scna_vcf_flag.py

"""
Usage:
    delly_scna_vcf_flag.py -v <vcfFile>  -o <outVCF> -c <outVCFconf> [-p <peDump>] [-s <scnaFile>]

Options:
  -h --help     Show this screen.
  -v --vcfFile  DELLY v0.5+
  -o --output   vcf file
  -c --outputConf   confident vcf file
  -p --peDump  paired end dump file 
  -s --scnaFile  CopyNumber file (optional)  
  

"""
from __future__ import division, print_function
from docopt import docopt
import vcf
import os, sys, re, csv
from collections import defaultdict
from itertools import islice
import numpy as np

arguments = docopt(__doc__)
#print arguments
#sys.exit(0)
vcfFile = arguments['<vcfFile>']

outVCF = arguments['<outVCF>']

outVCFconf = arguments['<outVCFconf>']

scnaFile = arguments['<scnaFile>']

dumpFile = arguments['<peDump>']

if scnaFile:
    scna_dict = defaultdict(list)
    with open(scnaFile, 'r') as scna_in:
        scna_reader = csv.reader(scna_in, delimiter="\t")
        for b in scna_reader:
            scna_dict[b[0]].append(b[1:])
else:
    print ("no SCNA file given\nWill not annotate copy number support")

dump_dict = defaultdict(list)
if dumpFile:
    try:
        with open(dumpFile, 'r') as dump_in:
            dump_reader = csv.reader(dump_in, delimiter="\t")
            for b in dump_reader:
                if b and len(b)>6:
                    dump_dict[b[0]].append(b[6])
    except Exception, e:
        pass
    print (dumpFile)


def scna_overlap(chrom, pos, scna_dict, d):
    """
    check if x is distance d from y0 or y1
    """
    if chrom in scna_dict.keys():
        for scna in scna_dict.get(chrom):
            if pos in range(int(scna[0])-int(d),int(scna[0])+int(d) ) or \
            pos in range(int(scna[1])-int(d),int(scna[1])+int(d) ):
                return True

def adjust2depth(rd, suppPairs):
    '''
    correct supporting pairs to the read depth ratio
    return updated supp pair cutoff
    rd = record.INFO['RDRATIO']
    '''
    if rd>2:
         suppPairs = round(rd*suppPairs)
    return suppPairs

#sampleID = os.path.basename(os.path.dirname(vcfFile))


svdict = defaultdict(list)
svfilter = list()
if vcfFile:
    vcf_reader = vcf.Reader(open(vcfFile), 'r', compressed=True) if vcfFile.endswith('.gz') else vcf.Reader(open(vcfFile), 'r', compressed=False)
    for record in vcf_reader:
        sv_len = 0
        if record.CHROM == record.INFO['CHR2']:
            sv_len = abs(int(record.POS) - int(record.INFO['END']))
        svdict[record.INFO['SVTYPE']].append(sv_len)

total_svs = sum(len(i) for i in svdict.itervalues())
if total_svs>200:
    for k,v in svdict.iteritems():
        if len(v)/total_svs > 0.95:
            h = np.histogram(v, bins=[0,10000, 100000000])
            if h[0][0]*0.9 > h[0][1]:
                print ("artefact detected for", k, len(v) ,"SVs out of", total_svs)
                svfilter.append(k)



if vcfFile:
    vcf_reader = vcf.Reader(open(vcfFile), 'r', compressed=True) if vcfFile.endswith('.gz') else vcf.Reader(open(vcfFile), 'r', compressed=False)
    if scnaFile:
        vcf_reader.infos['SCNA_SUPP'] = vcf.parser._Info('SCNA_SUPP', '0', 'Flag', 'somatic Copy number alteration switch at break end', '','')
    if dumpFile:
        vcf_reader.infos['READ_ID'] = vcf.parser._Info('READ_ID', '.', 'String', 'read names of supporting pairs', '','')
    vcf_writer = vcf.Writer(open(outVCF, 'w'), vcf_reader, lineterminator='\n')
    vcf_writer_conf = vcf.Writer(open(outVCFconf, 'w'), vcf_reader, lineterminator='\n')
    scna_dist = 50e3  
    for record in vcf_reader:
        suppPairs = 4
        overlap = False
        chrom1 = record.CHROM
        pos1 = record.POS
        suppPairs = adjust2depth(record.INFO['RDRATIO'], suppPairs)
        chrom2 = record.INFO['CHR2']
        pos2 = record.INFO['END']
        sv_len = 0
        if record.CHROM == record.INFO['CHR2']:
            sv_len = abs(int(record.POS) - int(record.INFO['END']))
        if scnaFile:
            if scna_overlap(chrom1, pos1, scna_dict, scna_dist):
                overlap = True
                record.INFO['SCNA_SUPP'] = True
            elif scna_overlap(chrom2, pos2, scna_dict, scna_dist):
                overlap = True
                record.INFO['SCNA_SUPP'] = True
        if dump_dict.has_key(record.ID):
            readname = [i for i in dump_dict.get(record.ID) if i]
            # sort alphabetically
            readname = sorted(readname)
            record.INFO['READ_ID'] = ','.join(map(str,readname))
        vcf_writer.write_record(record)
        if not record.FILTER and \
        (not [call for call in record.samples if call['FT'] != 'PASS'] and\
           int(record.INFO['MAPQ'])>30 and\
           int(record.INFO['PE']) > suppPairs and\
           (int(sv_len)==0 or int(sv_len)>1500)  or\
         record.INFO.has_key('SCNA_SUPP') or\
         record.INFO.has_key('PRECISE')) and not\
        (record.INFO['SVTYPE'] in svfilter and (sv_len > 0) and (sv_len < 10000)):
            #print chrom1, pos1, chrom2, pos2, record.FILTER, record.INFO, "\n"
            vcf_writer_conf.write_record(record)
    vcf_writer.close()
    vcf_writer_conf.close()



print ("Output vcf file with SCNA_SUPP tagging:\n\n{0}\n\n and filtered file:\n\n{1}".format(outVCF, outVCFconf))

