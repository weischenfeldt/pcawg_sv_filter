#!/bin/bash


DELLY_VCF=$1
#EMAIL=$2
PE_DUMP=$2
SCNA_BED=$3

#EMAIL=weischen@embl.de
if [[ $# -lt 2 ]]
then
echo -e "syntax: <delly.vcf> <read_name file (optional)> <SCNA file (optional)> "
exit -1
fi
DATE=$(date +"%Y%m%d")

DELLY_VERSION="1-0-0"


CODE_DIR=/opt/code
DELLY2BED="${CODE_DIR}/dellyVcf2Tsv.py"
GENCODE="./files/gencode_sort.v19.bed.gz"
DELLY_RN_RAW_FILE="${DELLY_VCF}.tmp1"
DELLY_RN_FILE="${DELLY_VCF}.tmp2"
DELLY_PCAWG_FILE_PRE="${DELLY_VCF}.tmp3"
NAME_BASE=$(echo $DELLY_VCF | awk -F'[.]' '{print $1}')
DELLY_PCAWG_FILE="${NAME_BASE}.embl-delly_${DELLY_VERSION}.${DATE}.somatic.sv.vcf.gz"

python $CODE_DIR/delly_scna_vcf_flag.py -v ${DELLY_VCF}  -o ${DELLY_RN_RAW_FILE} -c ${DELLY_RN_FILE}  -p ${PE_DUMP} #-s ${SCNA_FILE}
ls  -l ${DELLY_RN_FILE}
echo -e "\n>> reformat to two lines per SV <<\n"
## reformat to two lines per SV and add nt at breakpoint ##
python $CODE_DIR/dellyVcf2BndVcf.py -v ${DELLY_RN_FILE} -o ${DELLY_PCAWG_FILE_PRE} #-e ${EMAIL}

echo -e "\n>> remove tumor frq annotation and validate vcf<<\n"
### remove tumor frq annotation ##
cat ${DELLY_PCAWG_FILE_PRE} | grep -v '##INFO=<ID=SC_' | sed 's/;SC_[^;]*//g' | vcf-sort | bgzip -c  > ${DELLY_PCAWG_FILE}
vcf-validator ${DELLY_PCAWG_FILE}

echo -e "\n>> Make BEDPE <<\n"
## make BEDPE files ##
$DELLY2BED -v ${DELLY_RN_FILE} -o ${DELLY_PCAWG_FILE/.vcf*/.bedpe.txt} #-g ${GENCODE}

echo -e "\n>> package up<< \n"
## package up
md5sum ${DELLY_PCAWG_FILE} | awk '{print $1}' > ${DELLY_PCAWG_FILE}.md5

tabix -p vcf ${DELLY_PCAWG_FILE}

md5sum ${DELLY_PCAWG_FILE}.tbi | awk '{print $1}' > ${DELLY_PCAWG_FILE}.tbi.md5

echo -e "\n\t## done with ${DELLY_PCAWG_FILE} ##\n"

#rm ${DELLY_VCF}.tmp*
