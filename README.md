# PCAWG DELLY SV PoN filter

PCAWG workflow to filter SVs from DELLY calls using Panel of Normal samples (PoN)

DELLY VCF format is additionally converted to the BND format to conform with the [VCFv4.2](https://samtools.github.io/hts-specs/VCFv4.2.pdf) standard

## Build the image

```
git clone https://ffavero@bitbucket.org/weischenfeldt/pcawg_sv_filter.git
cd pcawg_sv_filter
docker build -t pcawg_sv_filter:latest .
```

## Dockstore

### Run dockstore cwl locally:
```
dockstore tool launch --entry Dockstore.cwl --local-entry --json sample_configs.local.json
```

### Run Dockstore remote cwl:


## Without Dockstore

### Command line interface

Within the docker container, the following command is set up to run the sv filter task::
```
usage: run_sv_filter.py [-h] --run-id RUN_ID --somatic DELLY_SOMA --germline
                        DELLY_GERM [--email EMAIL] --readname DELLY_READNAME
```
### From the docker run interface

The simpliest example is set up when all the input vcf files are in the same folder, eg: `/your/input_dir`, the docker command can be called as:
```
docker run -ti \
    -v /your/output_dir:/home/ubuntu/filter_calls \
    -v /your/input_dir:/home/ubuntu/data \
    pcawg_sv_filter:latest /bin/bash
    run_sv_filter.py \
    --run-id  0c7aca3f-e006-4de3-afc2-20b4f727d4fd \
    --somatic /home/ubuntu/data/delly.somatic.sv.vcf.gz \
    --germline /home/ubuntu /data/delly.germline.sv.vcf.gz \
    --readname /home/ubuntu /data/somatic.sv.readname.txt.tar.gz
```

This command execute the filter scripts inside the docker images, and save the results in the filter_calls directory.